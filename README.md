# Monolog extra

The aim of this module is to group any monolog processor or handler that
may be useful for a Drupal module but it meets a use case
that is too specific to have in the monolog module.
