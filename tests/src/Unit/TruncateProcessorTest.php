<?php

namespace Drupal\Tests\monolog_extra\Unit;

use Drupal\monolog_extra\Logger\Processor\TruncateMessagesProcessor;
use Drupal\Tests\UnitTestCase;
use Monolog\DateTimeImmutable;
use Monolog\Level;
use Monolog\LogRecord;

/**
 * @coversDefaultClass \Drupal\monolog_extra\Logger\Processor\TruncateMessagesProcessor
 * @group monolog
 */
class TruncateProcessorTest extends UnitTestCase {

  /**
   * Check that the Truncate MessageProcessor truncates the message.
   *
   * @dataProvider providerTestTruncateProcessor
   */
  public function testTruncateProcessor($truncate_limit, $message, $expected_length): void {
    $record = new LogRecord(
      datetime: new DateTimeImmutable(TRUE, new \DateTimeZone(date_default_timezone_get())),
      channel: 'test',
      level: Level::Error,
      message: $message,
      context: [],
      extra: [],
    );

    $processor = new TruncateMessagesProcessor($truncate_limit);
    $truncated_record = $processor($record);
    $message_length = strlen($truncated_record->message);
    self::assertEquals($expected_length, $message_length);
  }

  /**
   * Data provider for self::testTruncateProcessor().
   */
  public function providerTestTruncateProcessor() {
    return [
      [100, $this->generateRandomString(200), 103],
      [100, $this->generateRandomString(99), 99],
    ];
  }

  /**
   * Generate ramdom string with a length.
   */
  protected function generateRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
  }

}
