<?php

namespace Drupal\monolog_extra\Logger\Processor;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;

/**
 * Truncate message from LogRecord to a specific length.
 *
 * Use this processor to reduce the disk space that the logs
 * can occup.
 */
class TruncateMessagesProcessor implements ProcessorInterface {

  /**
   * Maximum value the record values must have.
   *
   * @var int
   */
  protected int $truncateLength;

  /**
   * Constructs the processor.
   *
   * @param int $truncate_length
   *   Truncate length.
   */
  public function __construct(int $truncate_length) {
    $this->truncateLength = $truncate_length;
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(LogRecord $record) {
    return $this->truncateRecord($record);
  }

  /**
   * Function used to truncate the records.
   *
   * It uses mb_substr in the case
   * the multibyte library is present.
   *
   * @return string
   *   Truncates the function.
   */
  protected function truncateFunction() {
    return function_exists('mb_substr') ? 'mb_substr' : 'substr';
  }

  /**
   * Truncate the record message.
   *
   * @param \Monolog\LogRecord $record
   *   Log record.
   *
   * @return \Monolog\LogRecord
   *   Record with every key truncated.
   */
  protected function truncateRecord(LogRecord $record) {

    if (strlen($record->message) > $this->truncateLength) {
      $truncate_value = $this->truncateFunction()($record->message, 0, $this->truncateLength) . '...';
      $record = $record->with(message: $truncate_value);
    }

    return $record;
  }

}
