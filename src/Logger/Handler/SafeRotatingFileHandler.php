<?php

namespace Drupal\monolog_extra\Logger\Handler;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Level;
use Monolog\LogRecord;
use Monolog\Logger;

/**
 * Safely creates the file handlers.
 *
 * In order to make the handler secure, it does:
 *   - Prevent fatal errors.
 *   - Auto creates the log folder in case it does not exist.
 *   - Logs in case logs folder can't be generated.
 */
class SafeRotatingFileHandler extends RotatingFileHandler {

  use StringTranslationTrait;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Error message when log file can't be written.
   *
   * @var string
   */
  public static $errorMessage;

  /**
   * SafeRotatingFileHandler constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   Logger.
   * @param string $path
   *   Path where the log files will be stored.
   * @param int $maxFiles
   *   Number of maximum files. Optional.
   */
  public function __construct(FileSystemInterface $fileSystem, LoggerChannelInterface $loggerChannel, string $path, int $maxFiles = 0) {
    $this->fileSystem = $fileSystem;
    $this->loggerChannel = $loggerChannel;
    parent::__construct($path, $maxFiles, Level::Debug, TRUE, 0664);
  }

  /**
   * Check that logs directory exists.
   *
   * @return bool
   *   Directory logs exists.
   */
  public function prepareLogDirectory() {
    $dir = dirname($this->filename);
    if ($this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY)) {
      $directory_ready = TRUE;
    }
    else {
      $this->loggerChannel->warning('Directory "%dir" for logs not exists or does not have the right permissions, please revise it.', ['%dir' => $dir]);
      $directory_ready = FALSE;
    }
    return $directory_ready;
  }

  /**
   * Check the handler can write.
   *
   * @return bool
   *   TRUE if logger can write logs.
   */
  protected function canWrite() {
    return $this->prepareLogDirectory() && $this->logFileCanBeCreated();
  }

  /**
   * Check the log file can be created.
   *
   * @return bool
   *   TRUE when log file can be created.
   */
  public function logFileCanBeCreated() {
    $url = $this->getUrl();
    $can_be_created = FALSE;
    set_error_handler([$this, 'customErrorHandler']);
    try {
      // Check if file already exists.
      $file_exist_before = file_exists($url);

      $stream = fopen($url, 'a');
      if (is_resource($stream)) {
        // If the stream was opened successfully,
        // it can be closed.
        fclose($stream);

        // Only remove the file if it did not exist before.
        if (!$file_exist_before) {
          unlink($url);
        }

        $can_be_created = TRUE;
      }
    }
    catch (\Exception $e) {
      // Handle exception if needed.
      self::$errorMessage = $e->getMessage();
    }

    restore_error_handler();
    if (!$can_be_created) {
      $this->loggerChannel->critical($this->t('The stream or file :file could not be opened: :message', [
        ':file' => $url,
        ':message' => self::$errorMessage,
      ]));
    }

    return $can_be_created;
  }

  /**
   * Custom handler to get the warning on open file.
   *
   * @param string $msg
   *   Message error.
   */
  public function customErrorHandler($msg) {
    self::$errorMessage = preg_replace('{^(fopen|mkdir)\(.*?\): }', '', $msg);
  }

  /**
   * {@inheritdoc}
   */
  protected function write(LogRecord $record) : void {
    if ($this->canWrite()) {
      parent::write($record);
    }
  }

  /**
   * Convert drupal stream wrapper into real path.
   */
  protected function getGlobPattern(): string {
    $pattern = parent::getGlobPattern();
    // Only alter the pattern if contains drupal stream wrapper.
    if (preg_match('#^(\w+://)(.+)#', $pattern, $matches)) {
      $stream_wrapper_prefix = $matches[1];
      $rest_of_pattern = $matches[2];

      // Convert the stream wrapper prefix to a real file system path.
      $real_path = $this->fileSystem->realpath($stream_wrapper_prefix);

      if ($real_path) {
        return $real_path . DIRECTORY_SEPARATOR . $rest_of_pattern;
      }
    }

    // If there's no stream wrapper, or if realpath fails,
    // return the original pattern.
    return $pattern;
  }

}
